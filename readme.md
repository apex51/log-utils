## LogUtils

从日志解析程序中独立出的模块,目前提供下面的功能:

- IPv4 解析:使用 [IPIP](https://www.ipip.net/) 提供的 IP 库映射到全球地区码和国内地区码;
- Agent 解析设备信息和 App 版本信息;

#### 使用方法

使用 git submodule 方法引入到项目中,修改 build.sbt 指向本项目中的 `lib/logutils/src/main/java` 和 `lib/logutils/src/main/scala` 目录。