package com.dxy.logutil

/**
  * Created by jianghao on 8/7/16.
  */


class IPParser extends Serializable {

  // set external file names
  val IP_PATH = "mydata4vipday2.datx"

  // initialize IPExt
  val ipExt = new IPExt
  ipExt.load(IP_PATH)

  // get the last ip from ips, or find invalid ip
  def verifyIP(ip: String): String =
    """\d+\.\d+\.\d+\.\d+$""".r.findFirstIn(ip) match {
      case Some(a) => {
        val result = a.asInstanceOf[String]
        result.split('.').exists(element => element.toInt > 255 && element.toInt < 0)
        match {
          case true => "NA"
          case false => result
        }
      }
      case None => "NA"
    }

  // get location detail from ip address
  def getLocation(ip: String): scala.collection.mutable.Map[String, String] = {
    val resultMap = scala.collection.mutable.Map[String, String]()
    resultMap("countryCode") = ""
    resultMap("cityId") = "000000"
    resultMap("provinceId") = "00"

    verifyIP(ip) match {
      case "NA" =>
      case a =>
        val result = ipExt.find(a)
        result(11) match {
          case "CN" => // 'CN' will definitely guarantee a 6-digit location code
            resultMap("countryCode") = "CN"
            resultMap("cityId") = result(9)
            resultMap("provinceId") = result(9).slice(0, 2)
          case a if a.matches("[A-Z]{2}") =>
            resultMap("countryCode") = a
          case _ =>
        }
    }
    resultMap
  }

}
