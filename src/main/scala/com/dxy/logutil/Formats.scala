package com.dxy.logutil

/**
  * Created by jianghao on 8/3/16.
  *
  */

// scala struct for hive scala
case class Browser(name: String, ver: String) // browser name & browser version

case class OS(name: String, ver: String)

case class DXYApp(ac: String, av: String)
