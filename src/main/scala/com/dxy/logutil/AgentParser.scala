package com.dxy.logutil

/**
  * Created by jianghao on 8/7/16.
  */

import org.uaparser.scala.{Client, Device, OS => UAOS, Parser, UserAgent}

class AgentParser extends Serializable {

  val parser = Parser.get

  def parseAgent(agent: String): (Browser, OS, String, String, Short, DXYApp) = {
    /**
      *  parse Agent
      */
    val Client(userAgent, os, device) = parser.parse(agent)

    // parse user agent
    val UserAgent(uaFamily, uaMajor, uaMinor, uaPatch) = userAgent
    // -> uaFamily is the browser name
    val browserVer = {
      var version = uaMajor.getOrElse("")
      if (uaMinor.isDefined) {
        version += "." + uaMinor.get
        if (uaPatch.isDefined) version += "." + uaPatch.get
      }
      version
    }

    // parse os
    val UAOS(osFamily, osMajor, osMinor, osPatch, osPatchMinor) = os
    // -> osFamily is the os name
    val osVer = {
      var version = osMajor.getOrElse("")
      if (osMinor.isDefined) {
        version += "." + osMinor.get
        if (osPatch.isDefined) {
          version += "." + osPatch.get
          if (osPatchMinor.isDefined) version += "." + osPatchMinor.get
        }
      }
      version
    }

    // parse device
    val Device(deviceFamily, deviceBrand, deviceModel) = device
    // -> deviceFamily is the device name and type
    // -> deviceBrand is the brand name

    // parse platform
    val platform: Short = {
      if (deviceFamily == "Spider") 5 // "spider"
      else if (agent.contains("Mobile")) {
        if (agent.contains("dxyapp_name/") & agent.contains("dxyapp_version/")) 4 // "app"
        else if (agent.contains("MicroMessenger")) 3 // "weixin"
        else 2 // "mobile"
      }
      else 1 // "pc"
    }

    // parse dxy_app
    val dxyPattern = raw".+ dxyapp_name/(\w+) dxyapp_version/([\w\.]+) .+".r
    val (ac, av) = agent match {
      case dxyPattern(a, b) => (a, b)
      case _ => ("", "")
    }

    (Browser(uaFamily, browserVer), OS(osFamily, osVer), deviceFamily, deviceBrand.getOrElse(""), platform, DXYApp(ac, av))
  }


}
